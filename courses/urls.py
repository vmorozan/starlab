"""courses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import *
from django.contrib import admin
from rest_framework.routers import *
from authentication.views import *
from library.views import *

router = DefaultRouter()
router.register(r'users', user_viewset)
router.register(r'roles', roles_viewset)
router.register(r'users/roles', userroles_viewset)
router.register(r'privileges', privileges_viewset)
router.register(r'roles/privileges', roleprivileges_viewset)
router.register(r'courses', courses_viewset)
router.register(r'categories', categories_viewset)
router.register(r'category', category_viewset)
router.register(r'usersub', usersub_viewset)
router.register(r'chapters', chapter_viewset)
router.register(r'lessons', lessons_viewset)
router.register(r'attachments', attachments_viewset)
router.register(r'attachment', attachment_viewset)
router.register(r'comments', comments_viewset)
router.register(r'course', course_viewset)
router.register(r'chapter', chapter_viewset)
router.register(r'lesson', lesson_viewset)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^courses/', include(router.urls,
                            namespace='users')),
    url(r'^api-auth/', include('rest_framework.urls',
                                namespace='rest_framework')),
]
