from django.db import models
from authentication.models import user

class categories(models.Model):
    categories = models.CharField(max_length=255)


class courses(models.Model):
    author = models.CharField(max_length=255, blank=True)
    category = models.ManyToManyField(categories)


class category(models.Model):
    language = models.CharField(max_length=255)
    category = models.ManyToManyField(categories)


class user_subscription(models.Model):
    user = models.ManyToManyField(user)
    course = models.ManyToManyField(courses)


class chapters(models.Model):
    chapter_num = models.IntegerField(default=0)
    course = models.ManyToManyField(courses)


class lessons(models.Model):
    address = models.CharField(max_length=255)
    chapter = models.ManyToManyField(chapters)


class attachments(models.Model):
    address = models.CharField(max_length=255)
    lesson = models.ManyToManyField(lessons)


class attachment(models.Model):
    language = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    atta = models.ManyToManyField(attachments)


class comments(models.Model):
    comment = models.CharField(max_length=255)
    author = models.ManyToManyField(user)
    lesson = models.ManyToManyField(lessons)


class course(models.Model):
    course = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    courses = models.ManyToManyField(courses)


class chapter(models.Model):
    language = models.CharField(max_length=255)
    chapter_text = models.CharField(max_length=255)
    more_chapters = models.ManyToManyField(chapters)
    

class lesson(models.Model):
    lesson = models.CharField(max_length=255)
    language = models.CharField(max_length=255)
    lessons = models.ManyToManyField(lessons)
