from django.contrib import admin
from .models import *

admin.site.register(courses)
admin.site.register(categories)
admin.site.register(category)
admin.site.register(user_subscription)
admin.site.register(chapters)
admin.site.register(lessons)
admin.site.register(attachments)
admin.site.register(attachment)
admin.site.register(comments)
admin.site.register(course)
admin.site.register(chapter)
admin.site.register(lesson)
