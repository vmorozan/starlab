from rest_framework import serializers
from . import models

class courses_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.courses


class categories_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.categories


class category_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.category


class usersub_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.user_subscription


class chapters_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.chapters


class lessons_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.lessons


class attachments_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.attachments


class attachment_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.attachment


class comments_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.comments


class course_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.course


class chapter_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.chapter

class lesson_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.lesson
