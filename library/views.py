from django.shortcuts import render
from rest_framework import viewsets
from . import models
from . import serializers


class courses_viewset(viewsets.ModelViewSet):
    queryset = models.courses.objects.all()
    serializer_class = serializers.courses_serializer


class categories_viewset(viewsets.ModelViewSet):
    queryset = models.categories.objects.all()
    serializer_class = serializers.categories_serializer


class category_viewset(viewsets.ModelViewSet):
    queryset = models.category.objects.all()
    serializer_class = serializers.category_serializer


class usersub_viewset(viewsets.ModelViewSet):
    queryset = models.user_subscription.objects.all()
    serializer_class = serializers.usersub_serializer


class chapters_viewset(viewsets.ModelViewSet):
    queryset = models.chapters.objects.all()
    serializer_class = serializers.chapters_serializer


class lessons_viewset(viewsets.ModelViewSet):
    queryset = models.lessons.objects.all()
    serializer_class = serializers.lessons_serializer


class attachments_viewset(viewsets.ModelViewSet):
    queryset = models.attachments.objects.all()
    serializer_class = serializers.attachments_serializer


class attachment_viewset(viewsets.ModelViewSet):
    queryset = models.attachment.objects.all()
    serializer_class = serializers.attachment_serializer


class comments_viewset(viewsets.ModelViewSet):
    queryset = models.comments.objects.all()
    serializer_class = serializers.comments_serializer


class course_viewset(viewsets.ModelViewSet):
    queryset = models.course.objects.all()
    serializer_class = serializers.course_serializer


class chapter_viewset(viewsets.ModelViewSet):
    queryset = models.chapter.objects.all()
    serializer_class = serializers.chapter_serializer


class lesson_viewset(viewsets.ModelViewSet):
    queryset = models.lesson.objects.all()
    serializer_class = serializers.lesson_serializer
