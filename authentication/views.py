from django.shortcuts import render
from rest_framework import viewsets
from . import models
from . import serializers


class user_viewset(viewsets.ModelViewSet):
    queryset = models.user.objects.all()
    serializer_class = serializers.users_serializer


class roles_viewset(viewsets.ModelViewSet):
    queryset = models.roles.objects.all()
    serializer_class = serializers.roles_serializer


class userroles_viewset(viewsets.ModelViewSet):
    queryset = models.user_roles.objects.all()
    serializer_class = serializers.userroles_serializer


class privileges_viewset(viewsets.ModelViewSet):
    queryset = models.privileges.objects.all()
    serializer_class = serializers.privileges_serializer


class roleprivileges_viewset(viewsets.ModelViewSet):
    queryset = models.role_privileges.objects.all()
    serializer_class = serializers.roleprivileges_serializer
