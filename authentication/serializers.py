from rest_framework import serializers
from . import models


class users_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.user


class roles_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.roles


class userroles_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.user_roles


class privileges_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.privileges


class roleprivileges_serializer(serializers.ModelSerializer):
    class Meta:
        exclude = ()
        model = models.role_privileges
