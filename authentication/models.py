from django.db import models

# Create your models here.

class user(models.Model):
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.EmailField()

    def __str__(self):
        return self.username


class roles(models.Model):
    role = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.role


class user_roles(models.Model):
    role = models.ManyToManyField(roles)
    user = models.ManyToManyField(user)



class privileges(models.Model):
    privilege = models.CharField(max_length=255)
    description = models.TextField(max_length=255)

    def __str__(self):
        return self.privilege


class role_privileges(models.Model):
    privilege = models.ManyToManyField(privileges)
    role = models.ManyToManyField(roles)

