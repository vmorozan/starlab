from django.conf.urls import url
from . import views
from rest_framework import routers

router.register(r'users', views.user_viewset)
