from django.contrib import admin
from .models import *


# Register your models here.
admin.site.register(user)
admin.site.register(roles)
admin.site.register(user_roles)
admin.site.register(privileges)
admin.site.register(role_privileges)
